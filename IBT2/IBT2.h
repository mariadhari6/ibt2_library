#ifndef IBT2_H
#define IBT2_H

#include <mbed.h>

class IBT2
{

    public:
        IBT2(PinName pinA, PinName pinB, double freq);
        void setSpeed(double speed);
        double getSpeed(void);
        
    private:
        PwmOut pin_A;
        PwmOut pin_B;
        double speed_;
        double periode_;
        void setPeriode();
        double freq_;

};

#endif