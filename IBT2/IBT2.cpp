#include "IBT2.h"
IBT2::IBT2(PinName pinA, PinName pinB, double freq):
           pin_A(pinA), pin_B(pinB){
               this->freq_ = freq;
               this->setPeriode();  
           }
void IBT2::setPeriode(){
    periode_ = 1/freq_;
    /*
        * frequenshi di ubah ke satuan second (detik)
    */
    periode_ = periode_*1000;
    /*
        *Satuan detik di ubah ke milisecond
    */
    pin_A.period_ms(periode_);
    pin_B.period_ms(periode_);
}
void IBT2::setSpeed(double speed){
    speed_ = speed;
    if (speed < 0)
    {
        speed = speed * -1;
        pin_A.write(0.0f);
        pin_B.write(speed);
    }
    else{
        pin_A.write(speed);
        pin_B.write(0.0f);
    }
}

double IBT2::getSpeed(){
    return speed_;
}